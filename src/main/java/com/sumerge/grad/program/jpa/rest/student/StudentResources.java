package com.sumerge.grad.program.jpa.rest.student;

import com.sumerge.grad.program.jpa.entity.Address;
import com.sumerge.grad.program.jpa.entity.Course;
import com.sumerge.grad.program.jpa.entity.Student;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static com.sumerge.grad.program.jpa.constants.Constants.PERSISTENT_UNIT;
import static java.util.logging.Level.SEVERE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("students")
public class StudentResources {

    private static final Logger LOGGER = Logger.getLogger(StudentResources.class.getName());

    @PersistenceContext(unitName = PERSISTENT_UNIT)
    private EntityManager em;

    @Context
    HttpServletRequest request;

    @GET
    public Response getAllStudents() {
        try {
            return Response.ok().
                    entity(em.createQuery("SELECT s FROM Student s", Student.class).getResultList()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e).
                    build();
        }
    }

    @GET
    @Path("/course/{cName}")
    public Response getStudentsByCourse(@PathParam("cName") String courseName) {
        try {
            return Response.ok().
                    entity(em.createQuery("SELECT s FROM Student  s JOIN s.courses c WHERE c.name = :cName", Student.class).setParameter("cName", courseName).getResultList()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e).
                    build();
        }
    }

    @GET
    @Path("/total_hours/{sId}")
    public Response getStudentCourseHours(@PathParam("sId") Long studentId) {
        try {
            return Response.ok().
                    entity(em.createQuery("SELECT SUM(c.hours) FROM Student  s JOIN s.courses c WHERE s.id = :sId", Student.class).setParameter("sId", studentId).getResultList()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e).
                    build();
        }
    }

    @GET
    @Path("/city/{cityName}")
    public Response getStudentsByCity(@PathParam("cityName") String cityName) {
        try {
            return Response.ok().
                    entity(em.createQuery("SELECT s FROM Student  s JOIN s.addresses ad WHERE ad.city = :cityName", Student.class).setParameter("cityName", cityName).getResultList()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e).
                    build();
        }
    }

    @GET
    @Path("/city_count/{cityName}")
    public Response getNumberStudentsByCity(@PathParam("cityName") String cityName) {
        try {
            return Response.ok().
                    entity(em.createQuery("SELECT count(s) FROM Student  s JOIN s.addresses ad WHERE ad.city = :cityName", Student.class).setParameter("cityName", cityName).getResultList()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e).
                    build();
        }
    }


    @PUT
    @Transactional
    public Response editAddress(Address address) {
        System.out.println("***************************************************************************************************");
        System.out.println(address.getId());
        System.out.println(address.getCountry());
        System.out.println(address.getCity());
        System.out.println(address.getStreetAddress());
        System.out.println("***************************************************************************************************");
        try {
            if (address.getId() == null)
                throw new IllegalArgumentException("Can't edit student since it does not exist in the database");

            em.merge(address);
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e).
                    build();
        }
    }

    @GET
    @Path("/search")
    public Response searchStudents(@QueryParam("name") String stdName, @QueryParam("course") String courseName,
                                   @QueryParam("country") String country,@QueryParam("city") String city){

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Student> criteriaQuery = criteriaBuilder.createQuery(Student.class);

        Root<Student> studentEntity = criteriaQuery.from(Student.class);

        criteriaQuery.select(studentEntity);

        List<Predicate> predicates = new ArrayList<>();

        if(stdName != null){
            predicates.add(criteriaBuilder.like(studentEntity.<String>get("name"),"%" + stdName + "%"));
        }

        if(courseName != null){
            Join<Student, Course> studentCourseJoinEntity = studentEntity.join("courses",JoinType.INNER);
            predicates.add(criteriaBuilder.equal(studentCourseJoinEntity.<String>get("name"),courseName));
        }

        Join<Student, Address> studentAddressJoinEntity = studentEntity.join("addresses",JoinType.INNER);

        if(country != null){
            predicates.add(criteriaBuilder.equal(studentAddressJoinEntity.<String>get("country"),country));
        }

        if(city != null){
            predicates.add(criteriaBuilder.equal(studentAddressJoinEntity.<String>get("city"),city));
        }


        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

        TypedQuery<Student> query = em.createQuery(criteriaQuery);


        return Response.ok().entity(query.getResultList()).build();
    }

}
