# JPA sample

This Project is part of a task to learn about JPA, JPQL and CriteriaBuilder.

## Run

* Run DataBase scripts to have used tables and data (or create your own data base considering table names).

* mvn clean install && java -jar target/jpa-sample-swarm.jar

## Use

http://localhost:8880/jpa-demo/students


* GET students by course name : "/course/{courseName}"

* GET total hours of a student by id : "/total_hours/{sId}"

* GET students by city name : "/city/{cityName}"

* GET number of students in a city by city name : "/city_count/{cityName}"

### Criteria Builder

Search for a student using a portion of his data "/search?{queryParam}={queryData}"